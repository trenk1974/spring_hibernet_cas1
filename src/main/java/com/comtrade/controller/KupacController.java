package com.comtrade.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.comtrade.entity.Kupac;
import com.comtrade.service.KupacService;


@Controller
public class KupacController {
	
	@Autowired
	private KupacService kupacService;
	
	//@RequestMapping(value ="/list", method = RequestMethod.GET)
	@GetMapping("/list")
	public String vrati(Model model) {
		model.addAttribute("kupac", kupacService.vratiKupce());
		return "lista-kupaca";		
	}
	
	@GetMapping("/dodajKupca")
	public String prikazFormeZaDodavanjeKupca(Model model) {
		
		Kupac kupac=new Kupac();
		model.addAttribute("kupac", kupac);
		
		return "kupac-forma";
	}
	@PostMapping("/sacuvajKupca")
	public String sacuvajKupca(@ModelAttribute("kupac") Kupac kupac) {
		
		kupacService.save(kupac);
		
		return "redirect:/list";		
	}
	@GetMapping("/updateKupaca")
	public String updateKupca(@RequestParam("kupacId") int id) {
		
		;
		
		return "redirect:/list";		
	}
}
