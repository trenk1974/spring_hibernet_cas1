package com.comtrade.service;

import java.util.List;

import com.comtrade.entity.Kupac;

public interface KupacService {
	public List<Kupac> vratiKupce();

	public void save(Kupac kupac);
	
	public Kupac getKupac(int id);
}
