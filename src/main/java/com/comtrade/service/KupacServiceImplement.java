package com.comtrade.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.comtrade.dao.KupacDao;
import com.comtrade.entity.Kupac;


@Service
public class KupacServiceImplement implements KupacService{
	
	@Autowired
	private KupacDao kupacDao;
	
	@Override
	@Transactional
	public List<Kupac> vratiKupce() {		
		return kupacDao.vratiKupce();
	}

	@Override
	@Transactional
	public void save(Kupac kupac) {
		kupacDao.save(kupac);
		
	}

	@Override
	@Transactional
	public Kupac getKupac(int id) {		
		return kupacDao.getKupac(id);
	}

}
