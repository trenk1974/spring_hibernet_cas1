package com.comtrade.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.comtrade.entity.Kupac;

@Repository
public class KupacDaoImplement implements KupacDao{
	
	@Autowired
	private SessionFactory sessionFactory;
	
	
	@Override
	public List<Kupac> vratiKupce() {
		Session session=sessionFactory.getCurrentSession();
		List<Kupac> list=session.createQuery("from Kupac", Kupac.class).getResultList();
		return list;
	}


	@Override
	public void save(Kupac kupac) {
		Session session=sessionFactory.getCurrentSession();
		session.save(kupac);		
	}


	@Override
	public Kupac getKupac(int id) {
		Session session=sessionFactory.getCurrentSession();
		Kupac kupac=session.get(Kupac.class, id);
		return kupac;
	}

}
