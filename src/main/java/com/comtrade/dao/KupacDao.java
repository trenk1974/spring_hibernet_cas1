package com.comtrade.dao;

import java.util.List;

import com.comtrade.entity.Kupac;

public interface KupacDao {
	public List<Kupac> vratiKupce();

	public void save(Kupac kupac);

	public Kupac getKupac(int id);
}
