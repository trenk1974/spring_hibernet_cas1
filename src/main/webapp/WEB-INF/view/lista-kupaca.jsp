<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<input type="button" value="Dodaj Kupca" onclick="window.location.href='dodajKupca'" class="add-button"> 
	<table>
		<tr>
			<th>Ime</th>
			<th>Prezime</th>
			<th>email</th>			
		</tr>
		<tr>
			<c:forEach var="temp" items="${kupac}">
			<c:url var="updateLink" value="/updateKupaca">
				<c:param name="kupacId" value="${temp.id}"/>
			</c:url>
				<tr>
					<td>${temp.ime}</td>
					<td>${temp.prezime}</td>
					<td>${temp.email}</td>
				</tr>
				<tr>
					<a href="${updateLink}">Update</a>
				</tr>
			</c:forEach>
		</tr>
	</table>
</body>
</html>